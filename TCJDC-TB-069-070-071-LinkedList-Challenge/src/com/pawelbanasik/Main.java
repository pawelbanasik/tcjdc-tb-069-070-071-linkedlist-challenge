package com.pawelbanasik;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

public class Main {
	
	private static List<Album> albums = new ArrayList<>();
	
	
	public static void main(String[] args) {

		

		Album album = new Album("Album A", "Artist A");
		album.newSong("song a", 240);
		album.newSong("song b", 240);
		album.newSong("song c", 240);
		album.newSong("song d", 240);
		album.newSong("song e", 240);
		album.newSong("song f", 240);
		album.newSong("song g", 240);
		album.newSong("song h", 240);

		albums.add(album);
		
		List<Song> playlist = new LinkedList<>();
		albums.get(0).addToPlaylist("song a", playlist);
		albums.get(0).addToPlaylist("song b", playlist);
		albums.get(0).addToPlaylist("song c", playlist);
		albums.get(0).addToPlaylist("song d", playlist);
		albums.get(0).addToPlaylist("song e", playlist);
		albums.get(0).addToPlaylist("song f", playlist);
		
		
		
		System.out.println(playlist.toString());
		play(playlist);
	}

	public static void play(List<Song> playlist) {

		Scanner sc = new Scanner(System.in);
		boolean quit = false;
		boolean goingForward = true;
		ListIterator<Song> listIterator = playlist.listIterator();
		printMenu();
		if (playlist.isEmpty()) {

		} else {
			System.out.println("Playing: " + listIterator.next().toString());
		}
		
		while (!quit) {
			int action = sc.nextInt();
			sc.nextLine();
			
		
			switch (action) {
			case 0:
				System.out.println("Exit player.");
				quit = true;
				break;
			case 1:
				if (!goingForward) {
					if (listIterator.hasNext()) {
						listIterator.next();
					}
					goingForward = true;
				}
				
				if (listIterator.hasNext()) {
					System.out.println("Now playing " + listIterator.next().toString());
				} else {
					System.out.println("End of list.");
				}
				
				break;

			case 2:
				if (goingForward) {
					if (listIterator.hasPrevious()) {
						listIterator.previous();
					}
					goingForward = false;

				}
				if (listIterator.hasPrevious()) {
					System.out.println("Now playing " + listIterator.previous().toString());
				} else {
					System.out.println("Start of list.");
				}
				break;

			case 3:
				printMenu();
				break;

			default:
				break;
			}
		}
	}

	private static void printMenu() {

		System.out.println("Avaialble actions: \npress ");
		System.out.println(
				"0 - to quit\n" + "1 - go to next \n" + "2 - go to previous\n" + "3 - print menu options");

	}
	
}

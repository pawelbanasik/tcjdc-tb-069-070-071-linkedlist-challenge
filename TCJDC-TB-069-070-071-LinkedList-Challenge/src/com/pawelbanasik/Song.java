package com.pawelbanasik;

public class Song {

	private String title;
	private double duration;

	public Song(String title, int duration) {
		this.title = title;
		this.duration = duration;
	}

	public String getTitle() {
		return title;
	}

	public double getDuration() {
		return duration;
	}

	@Override
	public String toString() {
		return "Song [" + title + " " +  duration + "]";
	}

	
	
}

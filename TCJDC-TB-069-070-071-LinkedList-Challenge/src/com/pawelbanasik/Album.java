package com.pawelbanasik;

import java.util.ArrayList;
import java.util.List;

public class Album {

	private String name;
	private String artist;
	private List<Song> songs;

	public Album(String name, String artist) {
		this.name = name;
		this.artist = artist;
		this.songs = new ArrayList<>();
	}

	public List<Song> getSongs() {
		return songs;
	}

	public boolean newSong(String songTitle, int duration) {
		if (findSong(songTitle) == null) {
			songs.add(new Song(songTitle, duration));
			return true;
		} else {
			return false;
		}
	}

	private Song findSong(String songTitle) {
		for (Song song : songs) {
			if (song.getTitle().equals(songTitle)) {
				return song;
			}
		}
		return null;
	}

	public boolean addToPlaylist(String songTitle, List<Song> playlist) {
		if (findSong(songTitle) != null) {
			playlist.add(findSong(songTitle));
			return true;
		} else {
			return false;
		}
	}

	public boolean addToPlaylist(int trackNumber, List<Song> playlist) {
		int index = trackNumber - 1;
		if (index > 0 && index < songs.size()) {
			playlist.add(songs.get(trackNumber));
			return true;	
		} else {
			return false;
		}
	}

}
